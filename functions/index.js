const functions = require('firebase-functions');

const http = require('http');
const admin = require('firebase-admin');
admin.initializeApp();


var options = {
    host: 'treasure-bonds-service.herokuapp.com',
    path: '/treasure-bonds',
    method: 'PUT'
}

exports.schedulerRequest = functions.pubsub.schedule('every 1 hours').onRun((context) => {
    http.request(options, null).end()
    return null;
});